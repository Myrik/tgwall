import requests
from django.core.management.base import BaseCommand

from TGWall.tg_helper import TG_BOT_URL
from telegram.models import Update


class Command(BaseCommand):
    help = 'Get data from telegram'

    @staticmethod
    def get_updates_json() -> dict:
        response = requests.get(TG_BOT_URL + 'getUpdates')
        return response.json()

    def handle(self, *args, **options):
        data = self.get_updates_json()
        print(data)
        if data.get('ok'):
            for update in data['result']:
                Update.from_json(update)
