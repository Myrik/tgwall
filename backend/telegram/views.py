from logging import getLogger

import requests
from django.http import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from TGWall.opengraph import opengraph
from TGWall.tg_helper import TG_WIDGET
from telegram.models import Post, User, PhotoSize, PostReaction

log = getLogger(__name__)


class UserWall(TemplateView):
    """
    Meta class for user wall view
    """

    template_name = "wall.html"

    def make_context(self, user: User):
        context = super().get_context_data()
        context['login_widget'] = TG_WIDGET
        context['user'] = user
        context['me'] = self.request.user
        if self.request.user.is_authenticated:
            context['following'] = self.request.user.following.filter(id=user.id).exists()
        else:
            context['following'] = False

        context['posts'] = Post.objects.filter(from_user=user, hidden=False)
        return context


class WallByUid(UserWall):
    """
    View for wall by id
    """

    def get_context_data(self, uid: int):
        return self.make_context(
            get_object_or_404(User, id=int(uid))
        )


class WallByName(UserWall):
    """
    View for wall by name
    """

    def get_context_data(self, name: str):
        return self.make_context(
            get_object_or_404(User, username=name)
        )


def image_redirect(request, photo_id: int):
    """
    Get image from tg by api

    :param request:
    :param photo_id:
    :return:
    """
    photo = PhotoSize.objects.get(id=photo_id)
    data = requests.get(photo.url())
    return HttpResponse(data.content, content_type=data.headers['content-type'])


def follow(request, uid: int):
    user = User.objects.get(id=uid)
    me: User = request.user
    try:
        me.follow(user)
        return JsonResponse({'status': 'ok', 'id': uid}, status=200)
    except Exception as e:
        log.info(e)
        return JsonResponse({'status': 'fail', 'exception': str(e)}, status=403)


def unfollow(request, uid: int):
    user = User.objects.get(id=uid)
    me: User = request.user
    try:
        me.unfollow(user)
        return JsonResponse({'status': 'ok', 'id': uid}, status=200)
    except Exception as e:
        log.info(e)
        return JsonResponse({'status': 'fail', 'exception': str(e)}, status=403)


class MetaScraperView(View):
    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @staticmethod
    def post(request):
        url = request.POST['url']

        try:
            rez = opengraph(url)
        except:
            return JsonResponse({})

        meta = {}
        for src, data in rez.items():
            meta.update(data)

        return JsonResponse(meta)


class PostAction(View):
    @staticmethod
    def delete(request, pid):
        """
        Remove (hide) post from wall

        :param request:
        :param pid:
        :return:
        """
        post = Post.objects.get(id=pid)
        if post.from_user != request.user:
            return JsonResponse({'status': 'fail'}, status=403)
        post.hidden = True
        post.save()
        return JsonResponse({'status': 'ok', 'id': pid}, status=200)

    @staticmethod
    def post(request, pid):
        """
        Reaction to post

        :param request:
        :param pid:
        :return:
        """
        uid = request.user.id
        value = int(request.POST['value'])

        reactions = PostReaction.react(uid, pid, value)
        return JsonResponse({
            'post': pid,
            'user': uid,
            'value': reactions
        })
