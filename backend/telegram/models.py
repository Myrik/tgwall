import hashlib
import re
from datetime import datetime
from logging import getLogger

import requests
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Count, Q

from TGWall.fields import JSONField
from TGWall.tg_helper import TG_BOT_URL, TG_FILE_URL

log = getLogger(__name__)


def catch_data(fun):
    def wrapped(*args, **kwargs):
        try:
            return fun(*args, **kwargs)
        except Exception as e:
            log.error(f'Error data: {args} {kwargs}')
            raise e

    return wrapped


class User(AbstractUser):
    """
    id 	            Integer 	Unique identifier for this user or bot
    is_bot 	        Boolean 	True, if this user is a bot
    first_name 	    String 	    User‘s or bot’s first name
    last_name 	    String 	    Optional. User‘s or bot’s last name
    username 	    String 	    Optional. User‘s or bot’s username
    language_code 	String 	    Optional. IETF language tag of the user's language
    """
    id = models.BigIntegerField(primary_key=True)
    is_bot = models.BooleanField(default=False)
    first_name = models.CharField('First name', max_length=255, blank=True)
    last_name = models.CharField('Last name', max_length=255, blank=True)
    username = models.CharField('User name', max_length=255, unique=True, db_index=True)
    language_code = models.CharField('Language code', max_length=255, null=True)

    password = models.CharField(max_length=128, default=None, null=True)

    following = models.ManyToManyField('User', related_name='followers')
    ban = models.BooleanField(default=False)

    photo_url = models.URLField(blank=True, null=True, default=None)
    auth_date = models.IntegerField(default=None, null=True, blank=True)

    @classmethod
    @catch_data
    def from_json(cls, data: dict):
        obj, created = cls.objects.get_or_create(
            id=data['id'],
            defaults={
                # Can lost first_name when channel user
                'first_name': data.get('first_name', ''),
                'last_name': data.get('last_name', ''),
                'username': data.get('username', data.get('title', str(data['id'])))
            }
        )

        need_save = False
        if data.get('language_code') and obj.language_code != data['language_code']:
            obj.language_code = data['language_code']
            need_save = True
        if data.get('is_bot') is not None and obj.is_bot != data['is_bot']:
            obj.is_bot = data['is_bot']
            need_save = True

        if need_save:
            obj.save()

        return obj

    def follow(self, user: 'User'):
        if not self.following.filter(id=user.id).exists():
            self.following.add(user)
            return
        raise Exception('Alredy follow')

    def unfollow(self, user: 'User'):
        if self.following.filter(id=user.id).exists():
            self.following.remove(user)
            return
        raise Exception('Is not follower')

    def __str__(self):
        if self.first_name:
            return "%s" % self.first_name
        elif self.username:
            return "%s" % self.username
        else:
            return "%d" % self.id


class Chat(models.Model):
    class Types:
        PRIVATE = 'private'
        GROUP = 'group'
        SUPERGROUP = 'supergroup'
        CHANNEL = 'channel'

        CHOICES = (
            (PRIVATE, 'Private'),
            (GROUP, 'Group'),
            (SUPERGROUP, 'Supergroup'),
            (CHANNEL, 'Channel'),
        )

    id = models.BigIntegerField(primary_key=True, unique=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    type = models.CharField(max_length=255, choices=Types.CHOICES)
    title = models.CharField(max_length=255, null=True, blank=True)

    def send(self, text):
        url = TG_BOT_URL + 'sendMessage'
        data = {
            'chat_id': self.id,
            'text': text
        }
        requests.post(url, data)

    @classmethod
    def from_json(cls, data):
        obj, created = cls.objects.get_or_create(
            id=data['id'],
            defaults={
                'user': User.from_json(data),
                'type': data['type'],
                'title': data.get('title', '')
            }
        )
        return obj

    def __str__(self):
        return "%s" % (self.title or self.user)


class Post(models.Model):
    id = models.BigIntegerField(primary_key=True)
    from_user = models.ForeignKey(
        User,
        related_name='posts',
        verbose_name='User',
        on_delete=models.CASCADE
    )
    date = models.DateTimeField()
    chat = models.ForeignKey(
        Chat,
        related_name='posts',
        on_delete=models.CASCADE
    )
    text = models.TextField(null=True, blank=True)

    forward = models.ForeignKey('Forward', on_delete=models.CASCADE, null=True)
    hidden = models.BooleanField(default=False)
    is_command = models.BooleanField(default=False)

    def photos(self):
        if self.has_media:
            return self.media_group.photos.all()
        return []

    def get_reaction(self):
        return Post.objects.filter(
            id=self.id
        ).annotate(
            pos=Count('reactions', filter=Q(reactions__value__gt=0)),
            neg=Count('reactions', filter=Q(reactions__value__lt=0))
        ).values('pos', 'neg').first()

    @property
    def has_media(self):
        return hasattr(self, 'media_group')

    @classmethod
    def from_json(cls, data):
        text = data.get('text', data.get('caption')) or ''
        date = datetime.fromtimestamp(data['date'])

        mediagroup = None
        obj = None
        created = False

        if not text and data.get('media_group_id'):
            mediagroup = MediaGroup.objects.filter(mediagroup_id=data['media_group_id']).first()
            if mediagroup:
                obj = mediagroup.post

        if obj is None:
            obj, created = cls.objects.get_or_create(
                id=data['message_id'],
                defaults={
                    'text': text,
                    'from_user': User.from_json(data['from']),
                    'chat': Chat.from_json(data['chat']),
                    'date': date,
                    'is_command': text.startswith('/')
                }
            )

        if obj.is_command:
            if obj.text == '/me':
                user = obj.from_user
                if user.username:
                    path = f'user/{user.username}'
                else:
                    path = f'user_id/{user.id}'
                obj.chat.send(settings.MAIN_URL + path)

        if data.get('photo'):
            photos = data['photo']
            one_photo = SinglePhoto.from_json(photos)

            if not mediagroup:
                if data.get('media_group_id'):
                    mediagroup, mg_created = MediaGroup.objects.get_or_create(
                        mediagroup_id=data['media_group_id'],
                        single=False,
                        defaults={'post': obj}
                    )
                else:
                    mediagroup, mg_created = MediaGroup.objects.get_or_create(
                        mediagroup_id=str(one_photo.id),
                        single=True,
                        defaults={'post': obj}
                    )

            mediagroup.photos.add(one_photo)

        if created:
            forward = {
                k[8:]: v for k, v in data.items()
                if k.startswith('forward_')
            }
            if forward:
                obj.forward = Forward.from_json(forward)
                obj.save()

        return obj

    def get_links(self):
        # Get all links from post
        re_link = re.compile(r'https?://(?:[\w_-]+(?:(?:\.[\w_-]+)+))(?:[\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?')
        links = re_link.findall(self.text)
        return links

    class Meta:
        ordering = ('-date',)

    def __str__(self):
        return "(%s,%s)" % (self.from_user, self.text or '(no text)')


class Forward(models.Model):
    class Types:
        CHAT = 'chat'
        USER = 'user'
        CHOICES = (
            (CHAT, 'chat'),
            (USER, 'user'),
        )

    type = models.CharField(max_length=16, choices=Types.CHOICES)
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    date = models.DateTimeField()

    @classmethod
    def from_json(cls, data):
        date = datetime.fromtimestamp(data['date'])
        if 'from_chat' in data:
            obj = cls.objects.create(
                chat=Chat.from_json(data['from_chat']),
                date=date,
                type=cls.Types.CHAT
            )
        elif 'from' in data:
            obj = cls.objects.create(
                user=User.from_json(data['from']),
                date=date,
                type=cls.Types.USER
            )
        else:
            return
        return obj


class MediaGroup(models.Model):
    mediagroup_id = models.CharField(max_length=255)
    single = models.BooleanField(default=True)
    post = models.OneToOneField(Post, on_delete=models.CASCADE, related_name='media_group')

    class Meta:
        unique_together = [('mediagroup_id', 'single')]


class SinglePhoto(models.Model):
    id = models.CharField(max_length=32, primary_key=True, unique=True)
    media_group = models.ManyToManyField(MediaGroup, related_name='photos')
    photos = models.ManyToManyField('PhotoSize', related_name='source')

    def sizes(self):
        return [(i.size, i.width, i.height) for i in self.photos.all()]

    @property
    def small(self) -> 'PhotoSize':
        photos = self.photos.all()
        if photos:
            return photos[0]

    @property
    def big(self) -> 'PhotoSize':
        photos = list(self.photos.all())
        if photos:
            return photos[-1]

    @classmethod
    def from_json(cls, data: list):
        ids = sorted([i['file_id'] for i in data])

        hash_text = hashlib.md5('|'.join(ids).encode()).hexdigest()
        one_photo, created = cls.objects.get_or_create(id=hash_text)
        if created:
            for photo in data:
                photo_obj = PhotoSize.from_json(photo)
                one_photo.photos.add(photo_obj)
        return one_photo


class PhotoSize(models.Model):
    """
    file_id 	String 	    Unique identifier for this file
    width 	    Integer 	Photo width
    height 	    Integer 	Photo height
    file_size 	Integer 	Optional. File size
    """
    id = models.CharField(max_length=255, primary_key=True)
    width = models.IntegerField()
    height = models.IntegerField()
    size = models.IntegerField()

    class Meta:
        ordering = ['size']

    @classmethod
    def from_json(cls, data):
        obj, created = cls.objects.get_or_create(
            id=data['file_id'],
            defaults={
                'size': data['file_size'],
                'height': data['height'],
                'width': data['width']
            }
        )
        return obj

    def url(self):
        if not hasattr(self, 'path'):
            file = requests.get(TG_BOT_URL + 'getFile?file_id=' + self.id).json()
            self.path = file['result']['file_path']
        return TG_FILE_URL + self.path

    class Meta:
        ordering = ('size',)


class Update(models.Model):
    """
    update_id 	            Integer 	        The update‘s unique identifier. Update identifiers start from a certain
                                                positive number and increase sequentially. This ID becomes especially
                                                handy if you’re using Webhooks, since it allows you to ignore repeated
                                                updates or to restore the correct update sequence, should they get out
                                                of order. If there are no new updates for at least a week, then
                                                identifier of the next update will be chosen randomly instead
                                                of sequentially.
    message 	            Message 	        Optional. New incoming message of any kind — text, photo, sticker, etc.
    edited_message 	        Message 	        Optional. New version of a message that is known to
                                                the bot and was edited
    channel_post 	        Message 	        Optional. New incoming channel post of any kind — text,
                                                photo, sticker, etc.
    edited_channel_post 	Message 	        Optional. New version of a channel post that is known to the bot
                                                and was edited.
    inline_query        	InlineQuery 	    Optional. New incoming inline query.
    chosen_inline_result 	ChosenInlineResult 	Optional. The result of an inline query that was chosen by a user and
                                                sent to their chat partner. Please see our documentation on the feedback
                                                collecting for details on how to enable these updates for your bot.
    callback_query      	CallbackQuery 	    Optional. New incoming callback query.
    shipping_query 	        ShippingQuery 	    Optional. New incoming shipping query.
                                                Only for invoices with flexible price.
    pre_checkout_query 	    PreCheckoutQuery 	Optional. New incoming pre-checkout query.
                                                Contains full information about checkout.
    poll 	                Poll 	            Optional. New poll state. Bots receive only updates about polls,
                                                which are sent or stopped by the bot.
    """
    id = models.IntegerField(primary_key=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True)
    raw = JSONField(indent=True, default=dict)

    @classmethod
    def from_json(cls, data):
        obj, created = cls.objects.get_or_create(id=data['update_id'])
        if created:
            obj.post = Post.from_json(data['message'])
            obj.raw = data
            obj.save()

        return obj


class PostReaction(models.Model):
    value = models.IntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='reactions')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='reactions')

    class Meta:
        unique_together = [('user', 'post')]

    @classmethod
    def react(cls, user_id: int, post_id: int, value: int):
        react, created = cls.objects.update_or_create(
            user_id=user_id,
            post_id=post_id,
            defaults={
                'value': value
            }
        )

        return react.post.get_reaction()


class News(models.Model):
    seen = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='news')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='news')

    class Meta:
        unique_together = [('user', 'post')]

    # TODO
    # def create_news(self):
    #     for f in self.following.all():
    #         posts = f.posts.exclude(news__user=self)
    #         for post in posts:
    #             News.objects.create()
