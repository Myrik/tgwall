from django.contrib import admin

# Register your models here.
from telegram.models import User, PhotoSize, Forward, Chat, Post, Update


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username')
    search_fields = ('id', 'username')


@admin.register(PhotoSize)
class PhotoAdmin(admin.ModelAdmin):
    list_display = ('id', 'size', 'width', 'height')


@admin.register(Forward)
class ForwardAdmin(admin.ModelAdmin):
    list_display = ('id',)


@admin.register(Chat)
class ChatAdmin(admin.ModelAdmin):
    list_display = ('id',)


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('id', 'date')


@admin.register(Update)
class UpdateAdmin(admin.ModelAdmin):
    list_display = ('id', 'post')
