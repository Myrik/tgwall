from django.conf.urls import url

from telegram.views import (
    WallByName,
    WallByUid,
    image_redirect,
    follow,
    unfollow,
    PostAction,
    MetaScraperView,
)

urlpatterns = [
    url(r'^user_id/(?P<uid>\d+)$', WallByUid.as_view()),
    url(r'^user/(?P<name>\w+)$', WallByName.as_view()),

    url(r'^image/(?P<photo_id>[\w\d\-_]+)$', image_redirect),
    url(r'^meta$', MetaScraperView.as_view()),

    url(r'^follow/(?P<uid>\d+)$', follow),
    url(r'^unfollow/(?P<uid>\d+)$', unfollow),

    url(r'^post/(?P<pid>\d+)$', PostAction.as_view())
]
