from django.conf.urls import url

from website.views import LoginView, LogoutView, OAuthView, SslForFreeView, FaviconView, Webhook

urlpatterns = [
    url(r'^logout$', LogoutView.as_view()),
    url(r'favicon\.ico', FaviconView.as_view()),
    url(r'\.well-known/acme-challenge/(?P<filename>[\w-]+)', SslForFreeView.as_view()),
    url(r'^OAuth$', OAuthView.as_view()),
    url(r'^webhook$', Webhook.as_view()),
    url(r'^$', LoginView.as_view()),
]
