import json
from logging import getLogger

from django.conf import settings
from django.contrib.auth import logout, login
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, RedirectView
from django_telegram_login.authentication import verify_telegram_authentication
from django_telegram_login.errors import TelegramDataIsOutdatedError, NotTelegramDataError

from TGWall.tg_helper import TG_WIDGET
from telegram.models import User, Update

log = getLogger(__name__)


class FaviconView(RedirectView):
    url = '/static/favicon.ico'
    permanent = True


class SslForFreeView(View):
    @staticmethod
    def get(request, filename):
        fsock = open(f'static/{filename}')
        return HttpResponse(fsock)


class LogoutView(View):
    @staticmethod
    def get(request):
        logout(request)
        return redirect('/')

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @staticmethod
    def post(request):
        data = f'WARNING: post to /\nbody = {request.body}'
        print(data)
        return HttpResponse('Not found', status=404)


class LoginView(TemplateView):
    """
    Login view
    """

    template_name = "login.html"

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('/user/' + self.request.user.username)
        context = self.get_context_data()
        response = self.render_to_response(context)
        return response

    def get_context_data(self):
        context = super().get_context_data()

        context['REDIRECT_LOGIN'] = TG_WIDGET
        context['TELEGRAM_BOT_NAME'] = settings.TELEGRAM_BOT_NAME

        return context


class OAuthView(View):
    @staticmethod
    def get(request):
        if not request.GET.get('hash'):
            return HttpResponse('Handle the missing Telegram data in the response.')

        try:
            result = verify_telegram_authentication(
                bot_token=settings.TELEGRAM_BOT_TOKEN,
                request_data=request.GET
            )
        except TelegramDataIsOutdatedError:
            return HttpResponse('Authentication was received more than a day ago.')
        except NotTelegramDataError:
            return HttpResponse('The data is not related to Telegram!')

        log.info(result)

        user_data = {
            'username': result.get('username'),
            'first_name': result['first_name'],
            'photo_url': result.get('photo_url'),
            'auth_date': result.get('auth_date')
        }

        me, created = User.objects.update_or_create(
            id=result['id'],
            defaults={
                k: v for k, v in user_data.items() if v
            }
        )

        if created:
            log.info(f'New user created: {result}')
        else:
            log.info(f'Login: {result}')

        login(request, me)

        return redirect('/')


class Webhook(View):
    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @staticmethod
    def post(request):
        body = json.loads(request.body)
        log.info(body)
        Update.from_json(body)
        return HttpResponse('ok', status=200)
