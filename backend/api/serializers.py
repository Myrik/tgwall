from logging import getLogger

from rest_framework import serializers

from TGWall.opengraph import opengraph
from telegram.models import Post, Forward, SinglePhoto, User

log = getLogger(__name__)


class ForwardSerializer(serializers.ModelSerializer):
    obj_id = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    username = serializers.SerializerMethodField()

    @staticmethod
    def get_username(obj: Forward):
        if obj.type == obj.Types.USER:
            return obj.user.username

    @staticmethod
    def get_obj_id(obj: Forward):
        if obj.type == obj.Types.CHAT:
            return obj.chat.id
        else:
            return obj.user.id

    @staticmethod
    def get_name(obj: Forward):
        if obj.type == obj.Types.CHAT:
            return obj.chat.title
        if obj.type == obj.Types.USER:
            return obj.user.get_full_name()

    class Meta:
        model = Forward
        fields = ('name', 'type', 'obj_id', 'username')


class PhotoSerializer(serializers.ModelSerializer):
    small = serializers.SerializerMethodField()
    big = serializers.SerializerMethodField()

    @staticmethod
    def get_big(obj: SinglePhoto):
        return obj.big.id

    @staticmethod
    def get_small(obj: SinglePhoto):
        return obj.small.id

    class Meta:
        model = SinglePhoto
        fields = ('id', 'small', 'big')


class PostSerializer(serializers.ModelSerializer):
    forward = ForwardSerializer(many=False)
    photos = PhotoSerializer(many=True)
    reaction = serializers.SerializerMethodField()
    my_reaction = serializers.SerializerMethodField()
    links = serializers.SerializerMethodField()

    def get_links(self, obj: Post):
        links = obj.get_links()
        ret = {}

        for i in links:
            try:
                og = opengraph(i, flat=True)
            except Exception as e:
                log.error(e)
                continue
            ret.update({i: og})

        return ret

    @staticmethod
    def my_reaction(obj: Post):
        return obj.my_reaction

    @staticmethod
    def get_reaction(obj: Post):
        return {
            'pos': obj.pos,
            'neg': obj.neg
        }

    class Meta:
        model = Post
        fields = ('id', 'text', 'date', 'forward', 'photos', 'reaction', 'from_user', 'links')


class UserSerializer(serializers.ModelSerializer):
    is_following = serializers.SerializerMethodField()

    @staticmethod
    def get_is_following(obj: User):
        try:
            return obj.is_following
        except AttributeError:
            return False

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'username', 'is_bot', 'ban', 'language_code', 'is_following')
