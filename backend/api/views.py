from django.db.models import Count, Q, OuterRef, Exists, Value, BooleanField, Subquery
from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination

from api.serializers import PostSerializer, UserSerializer
from telegram.models import Post, User, PostReaction


class UserPostViewSetPagination(PageNumberPagination):
    page_size = 15
    page_size_query_param = 'page_size'
    max_page_size = 50

    def paginate_queryset(self, queryset, request, view=None):
        request.build_absolute_uri = request.get_full_path
        return super().paginate_queryset(queryset, request, view)


class UserPostViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = PostSerializer
    pagination_class = UserPostViewSetPagination

    def get_queryset(self):
        user_id = self.request.query_params.get('uid', self.request.user.id)

        sub = PostReaction.objects.filter(post_id=OuterRef('pk'), user_id=self.request.user.id)

        return Post.objects.filter(
            from_user_id=user_id,
            is_command=False,
            hidden=False
        ).prefetch_related(
            'from_user', 'forward', 'chat', 'media_group__photos__photos',
            'forward__chat', 'forward__user'
        ).annotate(
            pos=Count('reactions', filter=Q(reactions__value__gt=0)),
            neg=Count('reactions', filter=Q(reactions__value__lt=0)),
            my_reaction=Subquery(sub.values('value')[:1])
        )


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = UserSerializer

    def get_queryset(self):
        if self.request.user.is_authenticated:
            following = User.objects.filter(
                id=OuterRef('pk'),
                followers=self.request.user,
            )
            rez = User.objects.annotate(
                is_following=Exists(following)
            )
            return rez
        return User.objects.filter().annotate(is_following=Value(0, BooleanField()))
