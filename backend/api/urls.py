from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from api.views import UserPostViewSet, UserViewSet

router = routers.DefaultRouter()
router.register('post', UserPostViewSet, base_name='UserPosts')
router.register('user', UserViewSet, base_name='Users')

urlpatterns = [
    url(r'^api/', include(router.urls)),
]
