from django.conf import settings
from django.contrib import admin
from django.urls import path, include

from api.urls import urlpatterns as api_urls
from telegram.urls import urlpatterns as telegram_urls
from website.urls import urlpatterns as website_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

if settings.DEBUG:
    import debug_toolbar

    print('Enable toolbar')
    urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]

urlpatterns += telegram_urls
urlpatterns += api_urls
urlpatterns += website_urls
