from TGWall.settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('POSTGRES_DB'),
        'USER': os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': 'database',
        'PORT': '5432',
    },
}

STATIC_ROOT = os.path.join(PROJECT_ROOT, '..', '..', 'static')
MEDIA_ROOT = os.path.join(PROJECT_ROOT, '..', '..', 'media')

STATIC_ROOT = os.environ.get('STATIC_ROOT') or STATIC_ROOT
MEDIA_ROOT = os.environ.get('MEDIA_ROOT') or MEDIA_ROOT

DEBUG = os.environ.get('DEBUG', False)
if DEBUG in (True, 'True'):
    DEBUG = True
else:
    DEBUG = False
