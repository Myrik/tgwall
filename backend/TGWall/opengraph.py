# coding=utf-8
import re

import requests
from parsel import Selector


def opengraph(url, flat=False):
    # type: (str) -> dict
    try:
        response = requests.get(url, stream=True)
    except:
        raise Exception('Cant get url')

    if not response.ok:
        raise Exception('Server status_code: %s' % response.status_code)
    if 'text/html' not in response.headers['content-type']:
        raise Exception('Server content type: %s' % response.headers['content-type'])

    items = Selector(
        text=response.text
    ).xpath(
        query='//meta[re:test(@name|@property, "^twitter|og:.*$", "i")]'
    ).getall()

    ret = {}

    name_re = re.compile(r' (?:name|property)=["\'](og|twitter):(.*?)["\']')
    content_re = re.compile(r' content=["\'](.*?)["\']')

    for item in items:
        t, name = name_re.findall(item)[0]
        content = content_re.findall(item)[0]
        ret_t = ret.get(t, {})
        ret_t.update({name: content})
        ret[t] = ret_t

    if flat:
        flat_data = {}
        for value in ret.values():
            flat_data.update(value)
        return flat_data

    return ret
