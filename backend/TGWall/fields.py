import json
from urllib.parse import urlsplit, urlunsplit

from django.contrib.postgres.fields.jsonb import JSONField as json_field
from django.contrib.postgres.forms.jsonb import InvalidJSONInput
from django.contrib.postgres.forms.jsonb import JSONField as json_form
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator
from django.db.models import URLField as django_urlfield
from django.forms import CharField, URLInput


class URLField(django_urlfield):
    def __init__(self, verbose_name=None, name=None, default_scheme='https', **kwargs):
        self.default_scheme = default_scheme
        super().__init__(verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        class FormClass(URLform):
            scheme = self.default_scheme

        return super().formfield(form_class=FormClass, **kwargs)


class URLform(CharField):
    scheme = 'https'
    widget = URLInput
    default_error_messages = {'invalid': 'Enter a valid URL.'}
    default_validators = [URLValidator()]

    def __init__(self, **kwargs):
        super().__init__(strip=True, **kwargs)

    def to_python(self, value):
        def split_url(url):
            try:
                return list(urlsplit(url))
            except ValueError:
                raise ValidationError(self.error_messages['invalid'], code='invalid')

        value = super().to_python(value)
        if value:
            url_fields = split_url(value)
            if not url_fields[0]:
                url_fields[0] = self.scheme
            if not url_fields[1]:
                url_fields[1] = url_fields[2]
                url_fields[2] = ''
                url_fields = split_url(urlunsplit(url_fields))
            value = urlunsplit(url_fields)
        return value


class JSONForm(json_form):
    ensure_ascii = False
    indent = None

    def prepare_value(self, value):
        if isinstance(value, InvalidJSONInput):
            return value
        return json.dumps(value, ensure_ascii=self.ensure_ascii, indent=self.indent)


class JSONField(json_field):
    def __init__(self, verbose_name=None, name=None, encoder=None, indent=None, ensure_ascii=False, **kwargs):
        self.indent = indent
        self.ensure_ascii = ensure_ascii
        super().__init__(verbose_name=verbose_name, name=name, encoder=encoder, **kwargs)

    def formfield(self, **kwargs):
        jsonform = type('JSONForm', (JSONForm,), {
            'ensure_ascii': self.ensure_ascii,
            'indent': self.indent
        })
        defaults = {'form_class': jsonform}
        defaults.update(kwargs)
        return super(JSONField, self).formfield(**defaults)
