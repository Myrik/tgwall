from django.conf import settings
from django_telegram_login.widgets.generator import create_redirect_login_widget

TG_BOT_URL = f"https://api.telegram.org/bot{settings.TELEGRAM_BOT_TOKEN}/"
TG_FILE_URL = f"https://api.telegram.org/file/bot{settings.TELEGRAM_BOT_TOKEN}/"
TG_WIDGET = create_redirect_login_widget(
    redirect_url=settings.TELEGRAM_LOGIN_REDIRECT_URL,
    bot_name=settings.TELEGRAM_BOT_NAME
)
